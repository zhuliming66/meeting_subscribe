/*
 * 项目名称:platform-plus
 * 类名称:SubscribeDetailInfoController.java
 * 包名称:com.platform.modules.subscribe.controller
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:35:08        李鹏军     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.subscribe.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.platform.common.annotation.SysLog;
import com.platform.common.utils.RestResponse;
import com.platform.modules.sys.controller.AbstractController;
import com.platform.modules.subscribe.entity.SubscribeDetailInfoEntity;
import com.platform.modules.subscribe.service.SubscribeDetailInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Controller
 *
 * @author 李鹏军
 * @date 2019-11-07 15:35:08
 */
@RestController
@RequestMapping("subscribe/detailinfo")
public class SubscribeDetailInfoController extends AbstractController {
	@Autowired
	private SubscribeDetailInfoService subscribeDetailInfoService;

	/**
	 * 查看所有列表
	 *
	 * @param params 查询参数
	 * @return RestResponse
	 */
	@RequestMapping("/queryAll")
	@RequiresPermissions("subscribe:detailinfo:list")
	public RestResponse queryAll(@RequestParam Map<String, Object> params) {
		List<SubscribeDetailInfoEntity> list = subscribeDetailInfoService.queryAll(params);
		return RestResponse.success().put("list", list);
	}

	/**
	 * 分页查询
	 *
	 * @param params 查询参数
	 * @return RestResponse
	 */
	@GetMapping("/list")
	@RequiresPermissions("subscribe:detailinfo:list")
	public RestResponse list(@RequestParam Map<String, Object> params) {
		params.put("userID", getUserId());
		Page page = subscribeDetailInfoService.queryPage(params);
		return RestResponse.success().put("page", page);
	}

	/**
	 * 根据主键查询详情
	 *
	 * @param id 主键
	 * @return RestResponse
	 */
	@RequestMapping("/info/{id}")
	@RequiresPermissions("subscribe:detailinfo:info")
	public RestResponse info(@PathVariable("id") String id) {
		SubscribeDetailInfoEntity subscribeDetailInfo = subscribeDetailInfoService.getById(id);
		return RestResponse.success().put("detailinfo", subscribeDetailInfo);
	}

	/**
	 * 新增
	 *
	 * @param subscribeDetailInfo subscribeDetailInfo
	 * @return RestResponse
	 */
	@SysLog("新增")
	@RequestMapping("/save")
	@RequiresPermissions("subscribe:detailinfo:save")
	public RestResponse save(@RequestBody SubscribeDetailInfoEntity subscribeDetailInfo) {

		subscribeDetailInfoService.add(subscribeDetailInfo);
		return RestResponse.success();
	}

	/**
	 * 修改
	 *
	 * @param subscribeDetailInfo subscribeDetailInfo
	 * @return RestResponse
	 */
	@SysLog("修改")
	@RequestMapping("/update")
	@RequiresPermissions("subscribe:detailinfo:update")
	public RestResponse update(@RequestBody SubscribeDetailInfoEntity subscribeDetailInfo) {

		subscribeDetailInfoService.update(subscribeDetailInfo);

		return RestResponse.success();
	}

	/**
	 * 根据主键删除
	 *
	 * @param ids ids
	 * @return RestResponse
	 */
	@SysLog("删除")
	@RequestMapping("/delete")
	@RequiresPermissions("subscribe:detailinfo:delete")
	public RestResponse delete(@RequestBody String[] ids) {
		subscribeDetailInfoService.deleteBatch(ids);

		return RestResponse.success();
	}

	@SysLog("取消预约")
	@RequestMapping("/unsubscribing")
	public RestResponse unsubscribing(@RequestBody String[] ids) {
		subscribeDetailInfoService.unsubscribingByIds(ids);

		return RestResponse.success();
	}

	/**
	 * 预约会议室
	 *
	 * @param
	 * @return
	 */
	@SysLog("预约会议室")
	@RequestMapping("/subscribingRoom")
	public RestResponse subscribingRoom(@RequestBody JSONObject params) {
		//检查会议室是否被别人占用
		if (subscribeDetailInfoService.checkRoomStatus(params)) {
			//true,可以添加
			params.put("userId",getUserId());
			if (subscribeDetailInfoService.saveEntityList(params)) {
				return RestResponse.success();
			}
			return RestResponse.error();
		}
		return RestResponse.error(1, "该会议室在此时间段已被占用");
	}


}
