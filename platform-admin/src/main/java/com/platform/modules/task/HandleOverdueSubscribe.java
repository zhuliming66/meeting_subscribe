package com.platform.modules.task;

import com.platform.modules.subscribe.entity.SubscribeDetailInfoEntity;
import com.platform.modules.subscribe.service.SubscribeDetailInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("handleOverdueSubscribe")
public class HandleOverdueSubscribe {
	@Autowired
	private SubscribeDetailInfoService subscribeDetailInfoService;

	public void handleSubscribe() {
		// 查询预约过期的预约记录
		List<SubscribeDetailInfoEntity> list = subscribeDetailInfoService.listOverdueSubscribe();
		if (list.size() > 0) {
			// 修改预约状态为已过期
			for (SubscribeDetailInfoEntity item : list) {
				item.setStatus("C");
			}
			// 修改记录
			subscribeDetailInfoService.updateBatchById(list);
		}
	}
}
