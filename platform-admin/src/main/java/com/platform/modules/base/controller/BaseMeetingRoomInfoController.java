/*
 * 项目名称:platform-plus
 * 类名称:BaseMeetingRoomInfoController.java
 * 包名称:com.platform.modules.base.controller
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:11:35        zlm     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.base.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.platform.common.annotation.SysLog;
import com.platform.common.utils.RestResponse;
import com.platform.modules.sys.controller.AbstractController;
import com.platform.modules.base.entity.BaseMeetingRoomInfoEntity;
import com.platform.modules.base.service.BaseMeetingRoomInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Controller
 *
 * @author zlm
 * @date 2019-11-07 15:11:35
 */
@RestController
@RequestMapping("base/meetingroominfo")
public class BaseMeetingRoomInfoController extends AbstractController {
	@Autowired
	private BaseMeetingRoomInfoService baseMeetingRoomInfoService;

	/**
	 * 查看所有列表
	 *
	 * @param params 查询参数
	 * @return RestResponse
	 */
	@RequestMapping("/queryAll")
	@RequiresPermissions("base:meetingroominfo:list")
	public RestResponse queryAll(@RequestParam Map<String, Object> params) {
		List<BaseMeetingRoomInfoEntity> list = baseMeetingRoomInfoService.queryAll(params);
		return RestResponse.success().put("list", list);
	}

	/**
	 * 分页查询
	 *
	 * @param params 查询参数
	 * @return RestResponse
	 */
	@GetMapping("/list")
	@RequiresPermissions("base:meetingroominfo:list")
	public RestResponse list(@RequestParam Map<String, Object> params) {
		Page page = baseMeetingRoomInfoService.queryPage(params);

		return RestResponse.success().put("page", page);
	}

	/**
	 * 根据主键查询详情
	 *
	 * @param id 主键
	 * @return RestResponse
	 */
	@RequestMapping("/info/{id}")
	@RequiresPermissions("base:meetingroominfo:info")
	public RestResponse info(@PathVariable("id") String id) {
		BaseMeetingRoomInfoEntity baseMeetingRoomInfo = baseMeetingRoomInfoService.getById(id);

		return RestResponse.success().put("meetingroominfo", baseMeetingRoomInfo);
	}

	/**
	 * 新增
	 *
	 * @param baseMeetingRoomInfo baseMeetingRoomInfo
	 * @return RestResponse
	 */
	@SysLog("新增")
	@RequestMapping("/save")
	@RequiresPermissions("base:meetingroominfo:save")
	public RestResponse save(@RequestBody BaseMeetingRoomInfoEntity baseMeetingRoomInfo) {
		baseMeetingRoomInfo.setCreatedBy(getUserId());
		baseMeetingRoomInfo.setUpdatedBy(getUserId());
		baseMeetingRoomInfo.setCreatedTime(new Date());
		baseMeetingRoomInfo.setUpdatedTime(new Date());
		baseMeetingRoomInfoService.add(baseMeetingRoomInfo);

		return RestResponse.success();
	}

	/**
	 * 修改
	 *
	 * @param baseMeetingRoomInfo baseMeetingRoomInfo
	 * @return RestResponse
	 */
	@SysLog("修改")
	@RequestMapping("/update")
	@RequiresPermissions("base:meetingroominfo:update")
	public RestResponse update(@RequestBody BaseMeetingRoomInfoEntity baseMeetingRoomInfo) {
		baseMeetingRoomInfoService.update(baseMeetingRoomInfo);

		return RestResponse.success();
	}

	/**
	 * 根据主键删除
	 *
	 * @param ids ids
	 * @return RestResponse
	 */
	@SysLog("删除")
	@RequestMapping("/delete")
	@RequiresPermissions("base:meetingroominfo:delete")
	public RestResponse delete(@RequestBody String[] ids) {
		baseMeetingRoomInfoService.deleteBatch(ids);

		return RestResponse.success();
	}

	/**
	 * 查询可用的会议室集合
	 *
	 * @param params 查询条件
	 * @return RestResponse
	 */
	@SysLog("查询符合要求的会议室集合")
	@RequestMapping("/getAvailableRoom")
	public RestResponse getAvailableRoom(@RequestBody JSONObject params) {
		Page page = baseMeetingRoomInfoService.listAvailableRoom(params);
		return RestResponse.success().put("page", page);
	}

}
