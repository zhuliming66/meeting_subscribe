package com.platform;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.platform.common.utils.DateUtils;
import com.platform.modules.base.entity.BaseMeetingRoomInfoEntity;
import com.platform.modules.base.service.BaseMeetingRoomInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlatformAdminApplicationTests {

	@Autowired
	private BaseMeetingRoomInfoService baseMeetingRoomInfoService;

	@Test
	public void test() {
	}

	@Test
	public void testListAvailableRoom() {
		JSONObject params = new JSONObject();
		params.put("startDate", "2018-11-08");
		params.put("endDate", "2018-11-18");
		params.put("startTime","08:00:00");
		params.put("endTime","09:00:00");
		params.put("unitBuilding", "");
		params.put("floor", "");
		params.put("roomNumber", "");
		params.put("personNumber", "");
		Page page = baseMeetingRoomInfoService.listAvailableRoom(params);
		for(Object entity:page.getRecords()){
			System.err.println(entity);
		}

	}

	@Test
	public void testDate() {
		//将开始日期和结束日期装换成Date类型
		Date start = DateUtils.stringToDate("2019-11-09", DateUtils.DATE_PATTERN);
		Date end = DateUtils.stringToDate("2019-11-19", DateUtils.DATE_PATTERN);
		//计算出预约的天数
		long betweenDate = (end.getTime() - start.getTime()) / (60 * 60 * 24 * 1000);
		System.err.println(betweenDate);

		List<Map<String, Object>> listDate = new LinkedList<>();
		for (int i = 0; i <= betweenDate; i++) {
			//获取预约的每一天
			Date everyDay = DateUtils.addDateDays(start, i);
			//再转换成字符串
			String strEveryDay = DateUtils.format(everyDay, DateUtils.DATE_PATTERN);
			String strStartEveryDayTime = strEveryDay + " " + "08:30:00";
			String strEndEveryDayTime = strEveryDay + " " + "09:00:00";
			Map<String, Object> map = new HashMap<>(2);
			map.put("startTime", DateUtils.stringToDate(strStartEveryDayTime, DateUtils.DATE_TIME_PATTERN));
			map.put("endTime", DateUtils.stringToDate(strEndEveryDayTime, DateUtils.DATE_TIME_PATTERN));
			listDate.add(map);
			System.err.println(map);
		}
	}
}
