/*
 * 项目名称:platform-plus
 * 类名称:SubscribeDetailInfoServiceImpl.java
 * 包名称:com.platform.modules.subscribe.service.impl
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:35:08        李鹏军     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.subscribe.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.platform.common.utils.DateUtils;
import com.platform.common.utils.Query;
import com.platform.modules.subscribe.dao.SubscribeDetailInfoDao;
import com.platform.modules.subscribe.entity.SubscribeDetailInfoEntity;
import com.platform.modules.subscribe.service.SubscribeDetailInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service实现类
 *
 * @author 李鹏军
 * @date 2019-11-07 15:35:08
 */
@Service("subscribeDetailInfoService")
public class SubscribeDetailInfoServiceImpl extends ServiceImpl<SubscribeDetailInfoDao, SubscribeDetailInfoEntity> implements SubscribeDetailInfoService {

	@Autowired
	private SubscribeDetailInfoDao subscribeDetailInfoDao;
	private List<Map<String,Object>> timeList;

	@Override
	public List<SubscribeDetailInfoEntity> queryAll(Map<String, Object> params) {
		return baseMapper.queryAll(params);
	}

	@Override
	public Page queryPage(Map<String, Object> params) {
		//排序
		//params.put("sidx", "T.id");
		//params.put("asc", false);
		Page<SubscribeDetailInfoEntity> page = new Query<SubscribeDetailInfoEntity>(params).getPage();
		return page.setRecords(baseMapper.selectSubscribeDetailInfoPage(page, params));
	}

	@Override
	public boolean add(SubscribeDetailInfoEntity subscribeDetailInfo) {
		return this.save(subscribeDetailInfo);
	}

	@Override
	public boolean update(SubscribeDetailInfoEntity subscribeDetailInfo) {
		return this.updateById(subscribeDetailInfo);
	}

	@Override
	public boolean delete(String id) {
		return this.removeById(id);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteBatch(String[] ids) {
		return this.removeByIds(Arrays.asList(ids));
	}

	@Override
	public boolean unsubscribingByIds(String[] ids) {
		Integer row = subscribeDetailInfoDao.unsubscribingByIds(ids);
		if (row > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 检查该会议室在此事件段是否被占用
	 *
	 * @param
	 * @return
	 */
	@Override
	public boolean checkRoomStatus(JSONObject params) {
		timeList = makeTimeList(params.get("startDate").toString(),params.get("endDate").toString(),params.get("startTime").toString(),params.get("endTime").toString());
		params.put("timeList", timeList);
		List<SubscribeDetailInfoEntity> list = subscribeDetailInfoDao.checkRoomStatus(params);
		if (list.size() > 0) {
			// 有冲突，不可以预约
			return false;
		}
		// 不冲突，可以预约
		return true;

	}

	@Override
	public boolean saveEntityList(JSONObject params) {
		List<SubscribeDetailInfoEntity> entityList = new LinkedList<>();
		for (Map<String, Object> item : timeList) {
			SubscribeDetailInfoEntity entity = new SubscribeDetailInfoEntity();
			entity.setUserId(params.get("userId").toString());
			entity.setRoomId(params.get("roomId").toString());
			entity.setStartTime(DateUtils.stringToDate(item.get("startTime").toString(), DateUtils.DATE_TIME_PATTERN));
			entity.setEndTime(DateUtils.stringToDate(item.get("endTime").toString(), DateUtils.DATE_TIME_PATTERN));
			entity.setStatus("A");
			entity.setCreatedBy(params.get("userId").toString());
			entity.setCreatedTime(new Date());
			entity.setUpdatedBy(params.get("userId").toString());
			entity.setUpdatedTime(new Date());
			entityList.add(entity);
		}
		return this.saveBatch(entityList);
	}

	private List<Map<String, Object>> makeTimeList(String startDate, String endDate, String beginTime, String overTime) {
		//将开始日期和结束日期装换成Date类型
		Date start = DateUtils.stringToDate(startDate, DateUtils.DATE_PATTERN);
		Date end = DateUtils.stringToDate(endDate, DateUtils.DATE_PATTERN);
		//计算出预约的天数
		long betweenDate = (end.getTime() - start.getTime()) / (60 * 60 * 24 * 1000);
		List<Map<String, Object>> listDate = new LinkedList<>();
		for (int i = 0; i <= betweenDate; i++) {
			//获取预约的每一天
			Date everyDay = DateUtils.addDateDays(start, i);
			//再转换成字符串拼接
			String strEveryDay = DateUtils.format(everyDay, DateUtils.DATE_PATTERN);
			String strStartEveryDayTime = strEveryDay + " " + beginTime;
			String strEndEveryDayTime = strEveryDay + " " + overTime;
			Map<String, Object> map = new HashMap<>(2);
			map.put("startTime", strStartEveryDayTime);
			map.put("endTime", strEndEveryDayTime);
			listDate.add(map);
		}
		return listDate;
	}

	@Override
	public List<SubscribeDetailInfoEntity> listOverdueSubscribe() {
		return subscribeDetailInfoDao.selectList(new QueryWrapper<SubscribeDetailInfoEntity>().lt("END_TIME",new Date()).eq("STATUS","A"));
	}
}
