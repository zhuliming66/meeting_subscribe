/*
 * 项目名称:platform-plus
 * 类名称:SubscribeDetailInfoService.java
 * 包名称:com.platform.modules.subscribe.service
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:35:08        李鹏军     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.subscribe.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.platform.common.utils.RestResponse;
import com.platform.modules.subscribe.entity.SubscribeDetailInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author 李鹏军
 * @date 2019-11-07 15:35:08
 */
public interface SubscribeDetailInfoService extends IService<SubscribeDetailInfoEntity> {

	/**
	 * 查询所有列表
	 *
	 * @param params 查询参数
	 * @return List
	 */
	List<SubscribeDetailInfoEntity> queryAll(Map<String, Object> params);

	/**
	 * 分页查询
	 *
	 * @param params 查询参数
	 * @return Page
	 */
	Page queryPage(Map<String, Object> params);

	/**
	 * 新增
	 *
	 * @param subscribeDetailInfo
	 * @return 新增结果
	 */
	boolean add(SubscribeDetailInfoEntity subscribeDetailInfo);

	/**
	 * 根据主键更新
	 *
	 * @param subscribeDetailInfo
	 * @return 更新结果
	 */
	boolean update(SubscribeDetailInfoEntity subscribeDetailInfo);

	/**
	 * 根据主键删除
	 *
	 * @param id id
	 * @return 删除结果
	 */
	boolean delete(String id);

	/**
	 * 根据主键批量删除
	 *
	 * @param ids ids
	 * @return 删除结果
	 */
	boolean deleteBatch(String[] ids);


	/**
	 * 根据ID取消预订
	 *
	 * @param ids
	 * @return
	 */
	boolean unsubscribingByIds(String[] ids);

	boolean checkRoomStatus(JSONObject params);

	boolean saveEntityList(JSONObject params);

	/**
	 * 查询过期的预约记录
	 *
	 * @return
	 */
	List<SubscribeDetailInfoEntity> listOverdueSubscribe();
}
