/*
 * 项目名称:platform-plus
 * 类名称:SubscribeDetailInfoDao.java
 * 包名称:com.platform.modules.subscribe.dao
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:35:08        李鹏军     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.subscribe.dao;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.platform.modules.subscribe.entity.SubscribeDetailInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Dao
 *
 * @author 李鹏军
 * @date 2019-11-07 15:35:08
 */
@Mapper
public interface SubscribeDetailInfoDao extends BaseMapper<SubscribeDetailInfoEntity> {

    /**
     * 查询所有列表
     *
     * @param params 查询参数
     * @return List
     */
    List<SubscribeDetailInfoEntity> queryAll(@Param("params") Map<String, Object> params);
    /**
     * 自定义分页查询
     *
     * @param page   分页参数
     * @param params 查询参数
     * @return List
     */
    List<SubscribeDetailInfoEntity> selectSubscribeDetailInfoPage(IPage page, @Param("params") Map<String, Object> params);

    /**
     * 取消预订
     * @param ids
     * @return
     */
    Integer unsubscribingByIds(String[] ids);

    /**
     * 检查会议室在选择的时间段中是否被占用
     * @param params
     * @return
     */
    List<SubscribeDetailInfoEntity> checkRoomStatus(@Param("params") JSONObject params);
}
