/*
 * 项目名称:platform-plus
 * 类名称:SubscribeDetailInfoEntity.java
 * 包名称:com.platform.modules.subscribe.entity
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:35:08        李鹏军     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.subscribe.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 *
 * @author 李鹏军
 * @date 2019-11-07 15:35:08
 */
@Data
@TableName("SUBSCRIBE_DETAIL_INFO")
public class SubscribeDetailInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private String id;
	/**
	 * 用户ID
	 */
	private String userId;
	/**
	 * 会议室ID
	 */
	private String roomId;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 预约状态
	 */
	private String status;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Date updatedTime;
	/**
	 * 删除标识
	 */
	private String delFlag;

	/**
	 * 房间名称
	 */
	@TableField(exist = false)
	private String roomName;
	/**
	 * 楼栋单元
	 */
	@TableField(exist = false)
	private String unitBuilding;
	/**
	 * 楼层
	 */
	@TableField(exist = false)
	private Integer floor;
	/**
	 * 房间号
	 */
	@TableField(exist = false)
	private String roomNumber;
	/**
	 * 容纳人数
	 */
	@TableField(exist = false)
	private Integer personNumber;
}
