/*
 * 项目名称:platform-plus
 * 类名称:BaseMeetingRoomInfoDao.java
 * 包名称:com.platform.modules.base.dao
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:11:35        zlm     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.base.dao;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.platform.modules.base.entity.BaseMeetingRoomInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Dao
 *
 * @author zlm
 * @date 2019-11-07 15:11:35
 */
@Mapper
public interface BaseMeetingRoomInfoDao extends BaseMapper<BaseMeetingRoomInfoEntity> {

    /**
     * 查询所有列表
     *
     * @param params 查询参数
     * @return List
     */
    List<BaseMeetingRoomInfoEntity> queryAll(@Param("params") Map<String, Object> params);

    /**
     * 自定义分页查询
     *
     * @param page   分页参数
     * @param params 查询参数
     * @return List
     */
    List<BaseMeetingRoomInfoEntity> selectBaseMeetingRoomInfoPage(IPage page, @Param("params") Map<String, Object> params);

    /**
     * 查询可用的会议室集合
     * @param params
     * @return
     */
    List<BaseMeetingRoomInfoEntity> listAvailableRoom(IPage page,@Param("params") JSONObject params);
}
