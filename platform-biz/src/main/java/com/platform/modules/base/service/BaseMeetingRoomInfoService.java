/*
 * 项目名称:platform-plus
 * 类名称:BaseMeetingRoomInfoService.java
 * 包名称:com.platform.modules.base.service
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:11:35        zlm     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.base.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.platform.modules.base.entity.BaseMeetingRoomInfoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author zlm
 * @date 2019-11-07 15:11:35
 */
public interface BaseMeetingRoomInfoService extends IService<BaseMeetingRoomInfoEntity> {

    /**
     * 查询所有列表
     *
     * @param params 查询参数
     * @return List
     */
    List<BaseMeetingRoomInfoEntity> queryAll(Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return Page
     */
    Page queryPage(Map<String, Object> params);

    /**
     * 新增
     *
     * @param baseMeetingRoomInfo 
     * @return 新增结果
     */
    boolean add(BaseMeetingRoomInfoEntity baseMeetingRoomInfo);

    /**
     * 根据主键更新
     *
     * @param baseMeetingRoomInfo 
     * @return 更新结果
     */
    boolean update(BaseMeetingRoomInfoEntity baseMeetingRoomInfo);

    /**
     * 根据主键删除
     *
     * @param id id
     * @return 删除结果
     */
    boolean delete(String id);

    /**
     * 根据主键批量删除
     *
     * @param ids ids
     * @return 删除结果
     */
    boolean deleteBatch(String[] ids);

    /**
     * 查询可用的会议室集合
     * @param params
     * @return
     */
    Page listAvailableRoom(@Param("params") JSONObject params);
}
