/*
 * 项目名称:platform-plus
 * 类名称:BaseMeetingRoomInfoEntity.java
 * 包名称:com.platform.modules.base.entity
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:11:35        zlm     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.base.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 *
 * @author zlm
 * @date 2019-11-07 15:11:35
 */
@Data
@TableName("BASE_MEETING_ROOM_INFO")
public class BaseMeetingRoomInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;
    /**
     * 房间名称
     */
    private String roomName;
    /**
     * 楼栋单元
     */
    private String unitBuilding;
    /**
     * 楼层
     */
    private Integer floor;
    /**
     * 房间号
     */
    private String roomNumber;
    /**
     * 容纳人数
     */
    private Integer personNumber;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 修改人
     */
    private String updatedBy;
    /**
     * 修改时间
     */
    private Date updatedTime;
    /**
     * 删除标识
     */
    private String delFlag;
}
