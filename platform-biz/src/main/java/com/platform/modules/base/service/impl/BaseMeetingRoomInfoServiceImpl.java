/*
 * 项目名称:platform-plus
 * 类名称:BaseMeetingRoomInfoServiceImpl.java
 * 包名称:com.platform.modules.base.service.impl
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-11-07 15:11:35        zlm     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.modules.base.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.platform.common.utils.DateUtils;
import com.platform.common.utils.Query;
import com.platform.modules.base.dao.BaseMeetingRoomInfoDao;
import com.platform.modules.base.entity.BaseMeetingRoomInfoEntity;
import com.platform.modules.base.service.BaseMeetingRoomInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service实现类
 *
 * @author zlm
 * @date 2019-11-07 15:11:35
 */
@Service("baseMeetingRoomInfoService")
public class BaseMeetingRoomInfoServiceImpl extends ServiceImpl<BaseMeetingRoomInfoDao, BaseMeetingRoomInfoEntity> implements BaseMeetingRoomInfoService {

	@Autowired
	private BaseMeetingRoomInfoDao baseMeetingRoomInfoDao;

	@Override
	public List<BaseMeetingRoomInfoEntity> queryAll(Map<String, Object> params) {
		return baseMapper.queryAll(params);
	}

	@Override
	public Page queryPage(Map<String, Object> params) {
		//排序
//        params.put("sidx", "T.id");
//        params.put("asc", false);
		Page<BaseMeetingRoomInfoEntity> page = new Query<BaseMeetingRoomInfoEntity>(params).getPage();
		return page.setRecords(baseMapper.selectBaseMeetingRoomInfoPage(page, params));
	}

	@Override
	public boolean add(BaseMeetingRoomInfoEntity baseMeetingRoomInfo) {
		return this.save(baseMeetingRoomInfo);
	}

	@Override
	public boolean update(BaseMeetingRoomInfoEntity baseMeetingRoomInfo) {
		return this.updateById(baseMeetingRoomInfo);
	}

	@Override
	public boolean delete(String id) {
		return this.removeById(id);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteBatch(String[] ids) {
		return this.removeByIds(Arrays.asList(ids));
	}

	@Override
	public Page listAvailableRoom(JSONObject params) {
		//TODO 组装时间集合
		params.put("timeList", makeTimeList(params.get("startDate").toString(),
				params.get("endDate").toString(),
				params.get("startTime").toString(),
				params.get("endTime").toString()));
		Page<BaseMeetingRoomInfoEntity> page = new Query<BaseMeetingRoomInfoEntity>(params).getPage();
		return page.setRecords(baseMeetingRoomInfoDao.listAvailableRoom(page, params));
	}

	public List<Map<String, Object>> makeTimeList(String startDate, String endDate, String beginTime, String overTime) {
		//将开始日期和结束日期装换成Date类型
		Date start = DateUtils.stringToDate(startDate, DateUtils.DATE_PATTERN);
		Date end = DateUtils.stringToDate(endDate, DateUtils.DATE_PATTERN);
		//计算出预约的天数
		long betweenDate = (end.getTime() - start.getTime()) / (60 * 60 * 24 * 1000);
		List<Map<String, Object>> listDate = new LinkedList<>();
		for (int i = 0; i <= betweenDate; i++) {
			//获取预约的每一天
			Date everyDay = DateUtils.addDateDays(start, i);
			//再转换成字符串拼接
			String strEveryDay = DateUtils.format(everyDay, DateUtils.DATE_PATTERN);
			String strStartEveryDayTime = strEveryDay + " " + beginTime;
			String strEndEveryDayTime = strEveryDay + " " + overTime;
			Map<String, Object> map = new HashMap<>(2);
			map.put("startTime", strStartEveryDayTime);
			map.put("endTime", strEndEveryDayTime);
			listDate.add(map);
		}
		return listDate;
	}

}
